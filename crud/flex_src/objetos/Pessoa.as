// ActionScript file
package objetos {
	
	[Bindable]
	[RemoteClass(alias="br.com.gisomar.model.Pessoa")]

	public class Pessoa {
		
		public function Pessoa(){
			
		}
		
		public var id:int;
		public var nome:String;
		public var cpf:String;
		public var email:String;
		public var telefoneCelular:String;
	}
}