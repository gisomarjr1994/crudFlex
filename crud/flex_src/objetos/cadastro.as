import mx.collections.ArrayCollection;
import mx.controls.List;
import mx.managers.PopUpManager;
import objetos.Pessoa;
import mx.rpc.events.ResultEvent;
import mx.controls.Alert;
import mx.rpc.events.FaultEvent;

[Bindable]
private var message:String;

[Bindable]
private var pessoas:ArrayCollection = new ArrayCollection();

[Bindable]
private var pessoa:Pessoa = new Pessoa(); 

public function limparCampos():void {
	campo_nome.text = "";
	campo_cpf.text = "";
	campo_email.text = "";
	campo_cel.text = "";
}

private function handleLoadResult(ev:ResultEvent):void {
	pessoas = ev.result as ArrayCollection;
}

private function handleFault(ev:FaultEvent):void {
	message = "Erro: " + ev.fault.faultCode + " \n " 
		+ "Detalhe: " + ev.fault.faultDetail + " \n "
		+ "Mensagem: " + ev.fault.faultString;
}

public function listarPacientes():void {	
	servico.getPessoas();
}

public function salvarPessoa():void {
	pessoa.nome = campo_nome.text; 
	pessoa.cpf = campo_cpf.text; 
	pessoa.email = campo_email.text;
	pessoa.telefoneCelular = campo_cel.text;
	
	if(pessoa.nome!= "" && pessoa.cpf != "" && pessoa.email != "" && pessoa.telefoneCelular != ""){
		servico.save(pessoa);		
		Alert.show("Paciente inserido com sucesso", "Inserir", Alert.OK, null, null, null, Alert.OK);
		limparCampos();	
		listarPacientes();
	}else{
		Alert.show("Preencha todos os campos", "Alerta", Alert.OK, null, null, null, Alert.OK);
	}
}