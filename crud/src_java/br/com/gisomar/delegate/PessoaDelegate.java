package br.com.gisomar.delegate;

import java.util.List;

import br.com.gisomar.model.Pessoa;
import br.com.gisomar.service.PessoaService;
import br.com.gisomar.service.PessoaServiceImpl;

public class PessoaDelegate {
	
	private Pessoa pessoa;
	
	private PessoaService pessoaService;
	
	private Integer id;
	
	public PessoaDelegate(){
		if(this.pessoa == null){
			this.pessoa = new Pessoa(); 
		}
		
		if(this.pessoaService == null){
			this.pessoaService = new PessoaServiceImpl(); 
		}
	}

	public void remove(Integer id){
		this.pessoa.setId(id);
		pessoaService.remove(this.pessoa);
		this.pessoa = new Pessoa();
	}
	
	public void save(Pessoa p){
		pessoaService.salva(p);
	}

	public void merge(Pessoa p){
		pessoaService.atualiza(p);
	}
	
	public void load(){
		this.pessoa = pessoaService.procura(this.id);
	}
	
	public List<Pessoa> getPessoas(){
		return pessoaService.lista();
	}

	public List<Pessoa> getPessoasByNome(String nome){ 
		List<Pessoa> lista = pessoaService.pesquisaPessoasByNome(nome);
	
		return lista;
	}
	
	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
}
