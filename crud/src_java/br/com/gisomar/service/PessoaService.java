package br.com.gisomar.service;

import java.util.List;

import br.com.gisomar.model.Pessoa;

public interface PessoaService extends BaseService<Pessoa>{
	public void salva(Pessoa p);

	public void remove(Pessoa p);
	
	public Pessoa procura(Integer id);

	public void atualiza(Pessoa p);
	
	public List<Pessoa> pesquisaPessoasByNome(String nome);
}